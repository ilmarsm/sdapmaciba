package coding;

import java.util.Scanner;

public class Quizz {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int[] answer = {3,1,2,2,1};

        String arr [][] = new String[5][4];
        int score = 0;

        arr[0][0] = "Kas ir Latvijas galvaspilsēta?";
        arr[0][1] = "Tokija";
        arr[0][2] = "Roma";
        arr[0][3] = "Rīga";

        arr[1][0] = "Kā sauc Latvijas varenāko upi?";
        arr[1][1] = "Daugava";
        arr[1][2] = "Gauja";
        arr[1][3] = "Amata";

        arr[2][0] = "Kā sauc latvijas premjerministru";
        arr[2][1] = "Vilis Lācis";
        arr[2][2] = "Krišjānis Kariņš";
        arr[2][3] = "Aldis Gobzems";

        arr[3][0] = "Kā sauca uzņēmumu, kurā Padomju laikā ražoja radioaparātus?";
        arr[3][1] = "RAF";
        arr[3][2] = "VEF";
        arr[3][3] = "LOL";

        arr[4][0] = "Kur latviešu tautasdziesmā tecēja gailītis?";
        arr[4][1] = "Ciemā";
        arr[4][2] = "Saeimā";
        arr[4][3] = "Everestā";

        System.out.print("Uzraksti šeit savu vārdu. Kā tevi sauc: ");
        String name = scanner.nextLine();


        for (int j = 0; j < arr.length; j++){
            System.out.println(arr[j][0]);

            for (int i = 1; i < arr[0].length; i++){
                System.out.println("\t" + i + ") " + arr[j][i]);
            }

            System.out.print("Atbilde: ");
            int answerInput = scanner.nextInt();

            if (answerInput == answer[j]) {
                score = score + 1;
                System.out.println(name +", tava atbilde ir pareiza!");
                System.out.println("----------------------------------------------");
                System.out.println("----------------------------------------------");
            } else {
                System.out.println("Diemžēl, " + name + ", tava atbilde ir ļoti nepareiza!");
                System.out.println("----------------------------------------------");
                System.out.println("----------------------------------------------");
            }
        }
        if (score > 0) {
            if (score == 1) {
                System.out.println(name + ", tu atbildēji pareizi 1 reizi!");
            } else {
                System.out.println(name + ", tu atbildēji pareizi " + score + " reizes!");
            }
        } else {
            System.out.println("Nu, " + name + ", diemžēl, tu neatbildēji pareizi ne vienu reizi!");
            System.out.println();
            System.out.println("Bet, vienalga, paldies, " + name + "!");
        }
    }
}
