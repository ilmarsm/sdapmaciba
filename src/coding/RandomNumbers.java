package coding;

import java.util.Scanner;

public class RandomNumbers {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int random = getRandom(10);
        System.out.println(random);

        System.out.print("Cik rizēs uzminēsi skaitli? ");
        int timesGuess = scanner.nextInt();
        int timesG = 0;

        System.out.println("Lūdzu, ievadiet skaitli no 0 līdz 100: ");
        boolean answer = false;


        do {
            int num = scanner.nextInt();
            timesG +=1;

            if (num == random) {
                System.out.println("Pareizi!");
                System.out.println("Tu uzminēji " + timesG + " reizēs");
                answer = true;
            } else {

                 answer = false;
                if (num < random) {
                    System.out.println("Par mazu");
                } else {
                    System.out.println("Par lielu");
                }

            }


        } while (answer == false && timesG < timesGuess );

        if(timesG == timesGuess) {
            System.out.println("\nČalīt, salielījies, tev viss beidzies");
        }

    }

    public static int getRandom(int max){
        return (int) (Math.random()*max);
    }

    /* * returns random integer between minimum and maximum range */
    public static int getRandomInteger(int maximum, int minimum){
        return ((int) (Math.random()*(maximum - minimum))) + minimum;
    }
}
