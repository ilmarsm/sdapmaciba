package coding;

import java.util.Arrays;
import java.util.Scanner;

public class VarArgs {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Lūdzu, norādiet savu vārdu: ");
        String vards   = scanner.nextLine();

        averageGrades(vards, 5,9,2,2,2,2,2,2,9,8,9,8,9);
    }

    static void averageGrades (String firstArg, int... grades) {
        System.out.println("Jūsu vārds ir: " + firstArg);
        float total = 0;
        for (int i = 0; i<grades.length; i++) {
            total += grades[i];

        }

        System.out.println("Jūsu vidējā atzīme ir: " + total/grades.length);


    }
}
