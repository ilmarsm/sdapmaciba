package coding;

import java.util.Scanner;

public class WeightCalculations {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Lūdzu, norādiet savu svaru kg: ");
        float weight = scanner.nextFloat();
        System.out.print("Lūdzu, norādiet savu augumu metros: ");
        float height = scanner.nextFloat();
        double bmi = Math.round((weight / Math.pow(height, 2)));
        System.out.println("Jūsu auguma un svara attiecība ir: " + bmi);

        if (bmi < 18.5) {
            System.out.println("Jūs esat kārns!");
        } else if (bmi >= 18.5 && bmi <= 24.9) {
            System.out.println("Jūs esat normāls!");
        } else if (bmi >= 25 && bmi <=29.9) {
            System.out.println("Jūs esat resns!");
        } else if (bmi > 30) {
            System.out.println("Džīzas!!!");
        }
    }
}