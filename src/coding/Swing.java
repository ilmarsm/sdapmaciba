package coding;

import javax.swing.*;
import java.awt.*;

public class Swing {
    public static void main(String[] args) {

    }
}

class gui {
    public static void main(String args[]) {

        //Creating the Frame
        JFrame frame = new JFrame("Mama pirmais GGUI logs");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 350);

        //Creating the MenuBar and adding components
        JMenuBar mb = new JMenuBar();
        JMenu m1 = new JMenu("Fails");
        JMenu m2 = new JMenu("Rdiģēt");
        JMenu m3 = new JMenu("Palīdziba");
        mb.add(m1);
        mb.add(m2);
        mb.add(m3);
        JMenuItem m11 = new JMenuItem("Atvērt");
        JMenuItem m12 = new JMenuItem("Saglabāt Kā");
        m1.add(m11);
        m1.add(m12);
        JMenuItem m21 = new JMenuItem("Krāsa");
        JMenuItem m22 = new JMenuItem("Fonti");
        m2.add(m21);
        m2.add(m22);
        JMenuItem m31 = new JMenuItem("Padomi");
        m3.add(m31);

        //Creating the panel at bottom and adding components
        JPanel panel = new JPanel(); // the panel is not visible in output
        JLabel label = new JLabel("Ievadi tekstu");
        JTextField tf = new JTextField(15); // accepts upto 10 characters
        JButton send = new JButton("Nosūtīt");
        JButton reset = new JButton("Dzēst");
        panel.add(label); // Components Added using Flow Layout
        panel.add(tf);
        panel.add(send);
        panel.add(reset);

        // Text Area at the Center
        JTextArea ta = new JTextArea();

        //Adding Components to the frame.
        frame.getContentPane().add(BorderLayout.SOUTH, panel);
        frame.getContentPane().add(BorderLayout.NORTH, mb);
        frame.getContentPane().add(BorderLayout.CENTER, ta);
        frame.setVisible(true);
    }
}
