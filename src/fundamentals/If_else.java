package fundamentals;

public class If_else {
    public static void main(String[] args) {

        int month = 3;

        System.out.println("Mūsdienu un senie latviešu mēnešu nosaukumi");

        if (month >=1 && month < 4) {
            System.out.println("Ir ziema");
        } else if (month >=4 && month < 7) {
            System.out.println("Ir pavasaris");
        } else if (month >=6 && month < 10) {
            System.out.println("Ir vasara");
        } else if (month >=9 && month <= 12) {
            System.out.println("Ir rudens");
        }
        if (month == 1) {
            System.out.println("Ir janvāris - Sala mēnesis");
        } else if (month == 2) {
            System.out.println("Ir februāris - Sveču mēnesis");
        } else if (month == 3) {
            System.out.println("Ir marts - Sērsnu mēnesis");
        } else if (month == 4) {
            System.out.println("Ir aprīlis - Sulu mēnesis");
        } else {
            System.out.println("Kāds cits mēnesis");
        }
        switch (month) {
            case 1:
                System.out.println("Ir janvāris - Sala mēnesis");
                break;
            case 2:
                System.out.println("Ir janvāris - Sveču mēnesis");
                break;
            case 3:
                System.out.println("Ir janvāris - Sērsnu mēnesis");
                break;
            case 4:
                System.out.println("Ir janvāris - Sulu mēnesis");
                break;
            default:
                System.out.println("Ir cits mēnesis");
                break;
        }

    }
}
