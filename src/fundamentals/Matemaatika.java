package fundamentals;

import java.util.Scanner;

public class Matemaatika {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Ludzu, ievadiet pirmo skaitli!");
        int n1 = scanner.nextInt();

        System.out.println("Ludzu, ievadiet otro skaitli!");
        int n2 = scanner.nextInt();
        System.out.println("Jūsu ievadītie skaitļi ir: " + n1 + " un " + n2);
        int summa = n1 + n2;
        int atņemshana = n1 - n2;
        int reizinaajums = n1 * n2;
        float e1 = (float)n1;
        float e2 = n2;
        float daliijums = e1 / e2;

        System.out.println("Saskaitot: " + n1 + " + " + n2 + " = " + summa);
        System.out.println("Atņemot: " + n1 + " - " + n2 + " = " + atņemshana);
        System.out.println("Reizinot: " + n1 + " * " + n2 + " = " + reizinaajums);
        System.out.println("Dalot: " + n1 + " / " + n2 + " = " + daliijums);

        int i = n1;

        do {
            System.out.println(i);
            i++;
        } while (i < n2);

//        while (i <= n2) {
//            System.out.println(i);
//            i++;
//        }

    }
}
