package fundamentals;

public class Arrays {
    public static void main(String[] args) {


//        // Vienas dimensijas masīvs
//        String names[] = new String[10];
//
//        names[0] = "Juris";
//        names[1] = "Pēteris";
//        names[2] = "Anna";
//        names[3] = "Zenta";
//        names[4] = "Aija";
//        names[5] = "Vilis";
//        names[6] = "Vitors";
//        names[7] = "Inese";
//
//        for(int i = 0; i < names.length; i++) {
//
//            if (names[i] == "Aija") {
//                System.out.println("Atradām Aiju!");
//            }
//
//            System.out.println("Elementa " + (i+1) + " numurs ir: " + names[i]);
//        }


        // Daudzdimensiju masīvs (šis ir divdimensiju masīvs)
        String[][] masiivs = new String[2][4];

        masiivs[0] = new String[] {"Anna", "Zenta", "Inese", "Juta", "Dženifera", "Dacīte", "Elīza"};
        masiivs[1] = new String[] {"Juris", "Pēteris", "Jānis", "Ivo", "Džonijs"};

        System.out.println(masiivs[0].length);
        System.out.println(masiivs[1].length);

        for(int i = 0; i < masiivs.length; i++) {

            for (int j = 0; j < masiivs[i].length; j++){
                System.out.print(masiivs[i][j] + " ");
            }

            System.out.println();
        }


    }
}


//    String[][] myArray = new String[2][];
//        myArray[0] = new String[]{"Alice", "has", "the", "cat","test"}; // creating the first row (index number 0)
//                myArray[1] = new String[]{"The", "cat", "has", "Kārlis","test"};  // creating the second row (index number 1)


//                for (int i = 0; i < myArray.length; i++) {
//        for (int j = 0; j < myArray[i].length; j++) {
//        System.out.print(myArray[i][j] + " ");
//        }
//        System.out.println();
//        }