package fundamentals.fiori;

public class Flower {
    private String color;
    private String name;
    private double length;
    private double price;
    private boolean inStore;

    public void setColor(String color) {
        this.color = color;
    }

     public void setName(String name) {
        this.name = name;
     }

     public void setLength(double length) {
        this.length = length;
     }

     public void setPrice(double price) {
        this.price = price;
     }

     public void setInStore(boolean inStore) {
        this.inStore = inStore;
     }

     public String getInStore() {
        String flag;
        if (inStore == true) {
            flag = "Ir veikalā";
        } else {
            flag = "Nav veikalā";
        }
        return flag;
     }

     public void getFlowerInfo() {
         System.out.println("Žiedu nosaukums: " + name);
         System.out.println("Žiedu krāsa: " + color);
         System.out.println("Žiedu garums: " + length);
         System.out.println("Žiedu cena: " + price);
         System.out.println(getInStore());
     }

}
