package fundamentals;

public class Jauna {
    public static void main(String[] args) {
        String name = "Uldis";
        printName(name);

        System.out.println(diff(10, 5));


        int result = diff(12, 6);
        System.out.println(result);
    }

    static void printName(String vards) {
        System.out.println(vards);
    }

    static int diff(int arg1, int arg2) {
        return arg1 - arg2;
    }
}
