package fundamentals.klases_siikaak;

public class Main {
    public static void main(String[] args) {

        Car car1 = new Car();
        car1.setBrand("Volvo XC90");
        car1.setColor("zila");
        car1.setMaxSpeed(220);
        car1.printCarParameters();

        Car car2 = new Car();
        car2.setBrand("Opel");
        car2.setColor("zaļa");
        car2.setMaxSpeed(140);
        car2.printCarParameters();

        Car mashiina = new Car();
        mashiina.setBrand("Volga");
        mashiina.setColor("balta");
        mashiina.setMaxSpeed(160);
        mashiina.printCarParameters();

    }
}
