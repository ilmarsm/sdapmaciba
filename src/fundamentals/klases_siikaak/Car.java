package fundamentals.klases_siikaak;

public class Car {
    private String color;
    private int maxSpeed;
    private String brand;

    public void setColor(String color) {
        this.color = color;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setMaxSpeed(int maxSpeed) {
        int realSpeed;

        if (maxSpeed >= 200) {
            realSpeed = maxSpeed - 20;
        } else {
            if (brand == "Opel") {
                realSpeed = maxSpeed - 40;
            } else {
                realSpeed = maxSpeed - 10;
            }
        }

        this.maxSpeed = realSpeed;
    }



    public void printCarParameters() {
        System.out.println(String.format("Mašīnas krāsa ir %s, maksimālais ātrums ir %d km/h, un marka ir %s", color, maxSpeed, brand));
    }
}
